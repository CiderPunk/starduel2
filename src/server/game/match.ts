import { IUser, IMatch } from "../serverinterfaces"

export class Match implements IMatch{
  constructor(protected readonly players:Array<IUser>){
    var first = true
    this.players.forEach((p:IUser)=>{
      p.SetMatch(this)
      p.InitGameComms(first)
      first = false
    })
  }

  RelaySignal(sender:IUser, data:any){
    //send to other user
    this.players.forEach((p)=>{
      if (p != sender){
        p.SendSignalGC(data)
      }
    })
    
  }

}