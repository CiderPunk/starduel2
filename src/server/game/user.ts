import * as Io from "socket.io";
import { EventNames, Comms,  } from "../../common/comms";
import { IGameManager, IUser, ILobby, IMatch } from "../serverinterfaces";
import { v1 as uuid} from "uuid"
import { Config } from "../config";
import md5 = require('md5');
import { JoinResult, GameState, ChallengeStatus } from "../../common/enums";
import { IChallenge } from "../../client/interfaces";
import { Match } from "./match";
import { Socket } from "net";

export class User implements IUser{


  public name:string = "Unnamed"
  public lobby:ILobby
  public LoggedIn:boolean = false
  public uuid:string
  public state:GameState
  public readonly blocks:Map<string,null>
  public readonly challenges:Map<string, number>
  protected match:IMatch = null

  constructor(protected readonly socket:Io.Socket, protected readonly owner:IGameManager){
    socket.on(EventNames.JoinRequest,(req:Comms.JoinRequest)=>{ this.ValidateJoin(req) })
    socket.on(EventNames.RegisterRequest,(req:Comms.RegisterRequest)=>{ this.Register(req) })
    socket.on(EventNames.JoinLobbyRequest,(req:Comms.JoinLobbyRequest)=>{ this.JoinLobby(req) })
    socket.on(EventNames.StatusUpdate, (req:Comms.ChangeStatus)=>{ this.UpdateStatus(req)})
    socket.on(EventNames.ChallengeRequest, ( req:Comms.Challenge)=>{ this.IssueChallenge(req)})
    socket.on(EventNames.ChallengeResponse, (req:Comms.Challenge) => { this.ChallengeResponse(req)})
    socket.on(EventNames.SignalGC, (req:Comms.SignalGameComm)=>{ this.match.RelaySignal(this, req.data)})
    socket.once("disconnect", ()=>{this.Disconnect()})
    this.lobby = null
    this.state = GameState.Wait
    this.blocks = new Map<string,null>()
    this.challenges = new Map<string,number>()
    //socket.send(this.uuid)
  }

  ValidateJoin(req:Comms.JoinRequest):void{
    //validate for missing values
    if (req.uuid === undefined){
      this.socket.emit(EventNames.JoinResponse, { result:JoinResult.NoAccount,  message: ""  } as Comms.JoinResponse)
      return
    }
    //validate hash
    const hash = md5(req.name + req.uuid + Config.salt)
    if (hash != req.hash){
      this.socket.emit(EventNames.JoinResponse, { result:JoinResult.AuthError, message:"Failed authentication test" } as Comms.JoinResponse)
      return
    }
    //check for duplicate sessions
    if (this.owner.HasUser(req.uuid)){
      this.socket.emit(EventNames.JoinResponse, { result:JoinResult.AlreadyConnected, message:"Already connected" } as Comms.JoinResponse)
      return
    }
    //success!
    this.uuid = req.uuid
    this.name = req.name
    this.LoggedIn = true
    //this.socket.emit(EventNames.JoinResponse, { success:true   } as Comms.JoinResponse)
    this.owner.AddUser(this)
    this.owner.JoinLobby(this, req.lobbyId)
  }

  UpdateStatus(req: Comms.ChangeStatus): any {
    this.state = req.state
    if (this.lobby != null){
      this.lobby.Notify()
    }
  }

  JoinLobby(req:Comms.JoinLobbyRequest){
    this.owner.JoinLobby(this, req.name)
  }

  Register(req:Comms.RegisterRequest){
    this.name = req.name
    //generate a new uuid
    this.uuid = uuid()
    //generate hash
    const hash = md5(this.name + this.uuid + Config.salt)
    //send it back
    this.socket.emit(EventNames.RegisterResponse, { name: this.name, uuid: this.uuid, hash: hash } as Comms.RegisterResponse )
  }
  
  Disconnect(){
    //leave lobby
    this.LeaveLobby()
    //leave server
    this.owner.RemoveUser(this)
    //hopefully GC this sucka
  }

  LeaveLobby(){
    if (this.lobby != null){
      //decline all pending challenges
      this.UpdateChallenges(true)
      
      //remove self
      this.lobby.RemoveUser(this)
      this.socket.leave(this.lobby.uuid)  
    }

  }

  MoveToLobby(lobby:ILobby):void{
    this.LeaveLobby()
    this.state = GameState.Lobby
    this.lobby = lobby
    lobby.AddUser(this)
    this.socket.join(lobby.uuid)  
    //send user lobby deets  
    this.socket.emit(EventNames.PushedLobby, lobby.NetDetails())
  }

  IssueChallenge(req: Comms.Challenge):void {
    //this.lobby.ChallengeUser(req.otherUuid, this)
    const target = this.lobby.GetUser(req.uuid)
    const result = target!= null ? 
      target.AddChallenge(this.uuid) : 
      ChallengeStatus.UserNotFound

    switch(result){
      case ChallengeStatus.Blocked:
      case ChallengeStatus.AlreadyChallenged:
      case ChallengeStatus.Disconnected:
      case ChallengeStatus.UserNotFound:
        this.SendChallengeResponse(target.uuid, result)
        break

    }
  }

  
  UpdateChallenges(discod:boolean = false){
    this.challenges.forEach((ttl,uuid, collection)=>{ 
      const challenger = this.lobby.GetUser(uuid)
      if (challenger == null){
        collection.delete(uuid)
      }
      if(discod){
        challenger.SendChallengeResponse(this.uuid, ChallengeStatus.Disconnected)
      }
      if (ttl < Date.now()){
        collection.delete(uuid)
        challenger.SendChallengeResponse(this.uuid, ChallengeStatus.TimeOut)
      } 
    })
  }

  /**
   * 
   * @param challenger add a new challenge from the specified used
   */
  AddChallenge(challenger: string): ChallengeStatus {
    this.UpdateChallenges()
    if (this.blocks.has(challenger)){
      return ChallengeStatus.Blocked
    }
    if (this.challenges.has(challenger)){
      return ChallengeStatus.Pending
    }
    const ttl = Date.now() + Config.challengeTTL
    this.challenges.set(challenger, ttl)
    this.socket.emit(EventNames.Challenged, { uuid:challenger, status:ChallengeStatus.Pending } as Comms.Challenge)
  }

  /**
   * handle challenge response from this user
   * @param req 
   */
  ChallengeResponse(req: Comms.Challenge): void {
    const challenge = this.challenges.get(req.uuid)
    //delete it
    this.challenges.delete(req.uuid)
    const challenger = this.lobby.GetUser(req.uuid)
    if (challenge != null && challenger!=null){
      if (req.status == ChallengeStatus.Accepted && challenger.state == GameState.Lobby){
        //start new match...
        const match = new Match([this, challenger])

      }
      else if (req.status == ChallengeStatus.Blocked){
        this.blocks.set(req.uuid, null)
      }
      challenger.SendChallengeResponse(this.uuid,req.status )
    }    
  }

  SetMatch(match:IMatch){
    this.match = match
  }

  InitGameComms(initiator: boolean): void {
    this.socket.emit(EventNames.InitGC,{initiator: initiator} as Comms.InitGameComm)
  }

  SendSignalGC(data: any): void {
    this.socket.emit(EventNames.SignalGC,{data: data} as Comms.SignalGameComm)
  }
  /**
   * send challenge response back to challenger
   * @param uuid 
   * @param response 
   */
  SendChallengeResponse(uuid:string, response: ChallengeStatus): void {
    this.socket.emit(EventNames.ChallengeResponse, { uuid:uuid, status:response } as Comms.Challenge)
  }
  /**
   * generate simplified user details for comms
   */
  NetDetails():Comms.UserDetails{
    return { 
      name: this.name,
      uuid:this.uuid,
      state:this.state 
    }
  }
}