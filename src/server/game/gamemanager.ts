
import * as Io from "socket.io"
import { User } from "./user";
import { IGameManager, IUser, ILobby } from "../serverinterfaces";
import { Lobby } from "./lobby";

export class GameManager implements IGameManager{
  HasUser(uuid: string): boolean {
    return this.players.has(uuid)
  }

  readonly lobbies:Map<string,ILobby>
  readonly players:Map<string, IUser>
  readonly defaultLobby:ILobby

  JoinLobby(user:IUser, lobbyid:string){
    let lobby = this.lobbies.get(lobbyid)
    if (lobby == null){
      lobby = this.defaultLobby
    }
    user.MoveToLobby(lobby)
    //lobby.AddUser(user)
  }

  AddUser(user:IUser) {
    this.players.set(user.uuid, user)
  }

  RemoveUser(user: IUser) {
    this.players.delete(user.uuid)
  }

  AddLobby(lobby:ILobby){
    this.lobbies.set( lobby.uuid, lobby)
  }

  constructor(public readonly io:SocketIO.Server) {
    //player connected
    this.io.on('connection', (socket:Io.Socket)=>{
      console.log(`user connected: ${socket.id}`)
      //create a new user record
      const user = new User(socket, this)
    })
    this.lobbies = new Map<string, ILobby>()
    this.players = new Map<string, IUser>()

    this.defaultLobby = new Lobby(this, "Learner Lobby")
    this.AddLobby(this.defaultLobby)
  }



}