import { ILobby, IUser, IGameManager, IChallenge } from "../serverinterfaces";
import { v1 as uuid} from "uuid"
import { stringify } from "querystring";

import { EventNames, Comms,  } from "../../common/comms";
import { SSL_OP_ALLOW_UNSAFE_LEGACY_RENEGOTIATION } from "constants";
import { GameState } from "../../common/enums";
import { Config } from "../config";
export class Lobby implements ILobby{


  GetUser(uuid: string): IUser {
    return this.users.get(uuid)
  }

  protected readonly users:Map<string,IUser>
  protected readonly challenges:Map<string, Array<IChallenge> >
  protected updatePending:boolean

  Notify():void{
    //triggers an update message in 1 second
    if (this.updatePending){
      return
    }  
    this.updatePending = true 
    setTimeout(()=>{
      this.owner.io.to(this.uuid).emit(EventNames.LobbyUpdate, this.NetDetails())
      this.updatePending = false
    },1000)
  }

  RemoveUser(user:IUser)  {
    this.users.delete(user.uuid)
    this.Notify()
    //let other users know
    //this.owner.io.to(this.uuid).emit(EventNames.PlayerLeave, user.NetDetails())
  }

  AddUser(user:IUser) {
    this.users.set(user.uuid, user)
    this.Notify()
    //send user any outstanding challenges
  }

  NetDetails():Comms.LobbyDetails{
    return { name: this.name, uuid:this.uuid, users:this.GetUsers()}
  }

  GetUsers(): Array<Comms.UserDetails>{
    return Array.from(this.users.values(), u=>u.NetDetails() )
  }

  ChallengeUser(target:string, challenger: IUser) :void{
    let t = new Date()
    t.setSeconds(t.getSeconds() + Config.challengeTTL)
    let userChallenges = this.challenges.get(target)
    if (userChallenges == undefined){
      userChallenges = new Array<IChallenge>()
      this.challenges.set(target, userChallenges)
    }
    if (!userChallenges.find(( c:IChallenge)=>{ return c.challenger == challenger.uuid })){
      userChallenges.push({ challenger: challenger.uuid, ttl:t  } as IChallenge)
    }  
  }

  public readonly uuid:string

  constructor(protected owner:IGameManager, public name:string){
    this.uuid = uuid()
    this.users = new Map<string, IUser>()
    this.challenges = new Map<string, Array<IChallenge>>()
  }

}