import { Comms } from "../common/comms";
import { IOctreeContainer } from "babylonjs";
import { GameState, ChallengeStatus } from "../common/enums";

export interface IGameManager{
  HasUser(uuid: string): boolean;
  JoinLobby(user:IUser, lobbyId: string):void
  AddUser(user:IUser):void
  RemoveUser(user:IUser):void
  io:SocketIO.Server
}

export interface ILobby{
  GetUser(uuid: string): IUser
  uuid:string
  name:string
  AddUser(user:IUser):void
  RemoveUser(user:IUser):void
  NetDetails():Comms.LobbyDetails
  Notify():void
  ChallengeUser(challengee:string, challenger:IUser):void
}

export interface IChallenge{
  uuid:string
  challenger:string
  ttl:Date
}

export interface IUser{
  SendSignalGC(data: any): void
  SendChallengeResponse(uuid:string,status:ChallengeStatus): void
  AddChallenge(challenger:string): void
  NetDetails():Comms.UserDetails
  uuid:string
  name:string
  MoveToLobby(lobby:ILobby)
  state:GameState
  InitGameComms(initiator: boolean): void
  SetMatch(match:IMatch):void
}

export interface IMatch{ 
  RelaySignal(sender:IUser, data:any)

}