
export class DebugRenderer{

  protected canvas:HTMLCanvasElement
  protected ctx: CanvasRenderingContext2D = null

  public constructor(protected world:planck.World){

    this.canvas = document.getElementById("debugCanvas") as HTMLCanvasElement;
    this.canvas.width = 1680;
    this.canvas.height = 1060;
    this.ctx = this.canvas.getContext('2d');
    this.ctx.save();
  }

  public setVisible(show:boolean){
    this.canvas.style.display = show ? " block" : "none"
    //$("#debugCanvas").toggle(show)
  }

  public render():void{
    var ctx = this.ctx;
    //ctx.resetTransform();
    ctx.setTransform(1,0,0,1,0, 0);
    this.ctx.clearRect(0,0,this.ctx.canvas.width, this.ctx.canvas.height);
    ctx.setTransform(1,0,0,1,this.canvas.width / 2, this.canvas.height / 2);
    ctx.strokeStyle = "pink"
    ctx.lineWidth = 1

    for(var body = this.world.getBodyList(); body; body = body.getNext() ){
      if (body.isActive()){
        ctx.save()
        ctx.translate(body.getPosition().x, body.getPosition().y)
        ctx.rotate(body.getAngle())
        for(var fix = body.getFixtureList(); fix; fix = fix.getNext()){
          var shape = fix.getShape();
          switch(shape.m_type){
            case  "circle":
              var circle = shape as planck.CircleShape
              ctx.strokeStyle = "pink"
              ctx.moveTo(circle.getCenter().x, circle.getCenter().y)
              ctx.beginPath()
              ctx.arc(0, 0, circle.m_radius , 0,2*Math.PI)
              ctx.lineTo(0,0)

              ctx.stroke()
      
              break;

            case "polygon":
              var poly = shape as planck.PolygonShape
              var verts = poly.m_vertices
              var last = verts[verts.length - 1];
              ctx.strokeStyle = "green"


              ctx.moveTo(last.x, last.y);
              for(var point of poly.m_vertices){
                ctx.lineTo(point.x,point.y)
              }

              ctx.stroke()

              break
            case "edge":
              var edge = shape as planck.EdgeShape
              break

            case "chain":
              var chain = shape as planck.ChainShape
              var verts = chain.m_vertices
              ctx.strokeStyle = "blue"      
              ctx.moveTo(verts[0].x, verts[0].y);
              for(var point of verts)  {
                ctx.lineTo(point.x,point.y)
              }

              ctx.stroke()

              break
          }
        }
        ctx.restore();
      }
    }
  }


}