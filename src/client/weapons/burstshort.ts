import { V2 } from "../helpers/v2";
import { IShooter, IWeapon } from "../interfaces";
import { Constants } from "../constants";

export interface IBurstShotSettings{
  cooldown?:number
  shotSpeed?:number
  shotDensity?:number
  burstSize?:number
  burstTime?:number
  burstSpread?:number
}

export class BurstShot implements IWeapon{

  static readonly scratch:V2 = new V2()

  protected static readonly defaultSettings:IBurstShotSettings = {
    cooldown:1.5,
    shotSpeed:150,
    shotDensity:0.1,
    burstSize:5,
    burstTime:0.04,
    burstSpread:0.1
  }
  
  protected readonly settings:IBurstShotSettings
  protected cooling:number = 0
  protected fireDelay:number = 0
  protected toFire:number = 0

  public constructor(protected owner:IShooter, customSettings:IBurstShotSettings){
    this.settings = { ...BurstShot.defaultSettings, ...customSettings}
  }

  public SetOwner(ent: IShooter){ 
    this.owner = ent
  }

  public Update(firing: boolean) {
    this.cooling -=  Constants.TickTime
    if (this.cooling <= 0 ){ 
      if (firing ){
        this.cooling += this.settings.cooldown
        this.toFire = this.settings.burstSize
      }
      else{
        this.cooling = 0 
      }
    }
  
    if (this.toFire > 0){
      this.fireDelay -= Constants.TickTime
      if (this.fireDelay < 0){
        this.fireDelay+=this.settings.burstTime
        this.Fire()
        this.toFire--
      }
    }
  }

  Fire(): void {
    const angle = this.owner.ang() + (this.settings.burstSpread > 0 ? this.settings.burstSpread * (Math.random() - 0.5) : 0)
    BurstShot.scratch.setAngle(angle, this.settings.shotSpeed).add(this.owner.vel())
    let bullet = this.owner.owner.bulletPool.GetNew()
    bullet.Init(this.owner.pos(),BurstShot.scratch, this.settings.shotDensity, this.owner)
  }



}