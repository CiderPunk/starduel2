import { BaseWeapon, IBaseWeaponSettings } from "./baseweapon";
import { V2 } from "../helpers/v2";
import { IShooter } from "../interfaces";


export interface ICannonSettings extends IBaseWeaponSettings{
  cooldown?:number,
  shotSpeed?:number,
  shotDensity?:number,
}


export class Cannon extends BaseWeapon<ICannonSettings>{

  protected static readonly defaultSettings:ICannonSettings ={
    cooldown:0.2,
    shotSpeed:100,
    shotDensity:1,
  }

  public constructor(owner:IShooter, custom:ICannonSettings = null){
    super(owner, custom, Cannon.defaultSettings)
  }

  static readonly scratch:V2 = new V2()

  Fire(): void {
    Cannon.scratch.setAngle(this.owner.ang(), this.settings.shotSpeed).add(this.owner.vel())
    let bullet = this.owner.owner.bulletPool.GetNew()
    bullet.Init(this.owner.pos(),Cannon.scratch, this.settings.shotDensity, this.owner)
  }



}