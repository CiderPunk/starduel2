import { IWeapon, IEntity, IShooter } from "../interfaces";
import { Constants } from "../constants";

export interface IBaseWeaponSettings{
  cooldown?:number
}


export abstract class BaseWeapon<T extends IBaseWeaponSettings> implements IWeapon{

  protected timeToFire:number = 0
  protected readonly settings:T

  public constructor(protected owner:IShooter, customSettings:any, baseSettings:any){
    this.settings =  { ...baseSettings, ...customSettings}
  }

  public SetOwner(ent:IShooter):void{
    this.owner = ent;
  }

  abstract Fire():void

  public Update(firing: boolean) {
    this.timeToFire -=  Constants.TickTime
    if (this.timeToFire <= 0 ){ 
      if (firing ){
        this.Fire()
        this.timeToFire += this.settings.cooldown
      }
      else{
        this.timeToFire = 0
      }
    }
  
  }
}