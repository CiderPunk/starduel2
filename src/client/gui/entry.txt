import { h, Component,render, Attributes, ComponentChildren, ComponentChild, Ref } from "preact"
import { IClient } from "../interfaces";

export interface EntryProps{
  name:string
  owner:IClient
}

interface EntryState{
  name:string
  owner:IClient
}

export class Entry extends Component<EntryProps, EntryState>{

  render(props?: Readonly<EntryProps>,
     state?: Readonly<EntryState>, 
     context?: any): ComponentChild {
      return (
        <div class="container">
          <h1 class="title">{props.name}</h1>
          <button class="button"  onClick={e=> this.state.owner.StartGame() }>Start game!</button>
        </div>
        )
        
  }

  constructor(props: EntryProps) {
    super(props);
    this.state = { name: props.name, owner: props.owner };
  }
}