import { h, Component,render, Attributes, ComponentChildren, ComponentChild, Ref } from "preact"
import { Comms } from "../../common/comms";
import { GameState } from "../../common/enums";

export interface PlayerListProps{
  players:Array<Comms.UserDetails>
  localUuid:string
  onChallenge:(uuid:string)=>void
} 
export interface PlayerListState{
  //playerChallenge:(uuid:string)=>void

}

interface PlayerViewProps {
  player:Comms.UserDetails,
  onclick:(uuid:string)=>void,
  local:string,
}


const PlayerViewRow = (props:PlayerViewProps)=>{
  const self = props.player.uuid == props.local
  return(
  <tr>
    <th>{props.player.name}</th>
    <td width="20%">{StateToName(props.player.state)}</td>
    <td width="20%">
      {self ?
       <span>(You)</span> : <a class="button is-small is-fullwidth" onClick={e=>{props.onclick(props.player.uuid)}}>Challenge</a>
      }
    </td>
  </tr>)}


const PlayerView = (props:PlayerViewProps)=>{
  const self = props.player.uuid == props.local
  return(
  <li class="box">
    <div class="columns">
      <div class="column is-half"><strong>{props.player.name}</strong></div>
      <div class="column is-one-quarter">{StateToName(props.player.state)}</div>
      {self ?
       <div class="column">(You)</div>
       :
      <div class="column">
        <a class="button is-small" onClick={e=>{props.onclick(props.player.uuid)}}>Challenge</a>
      </div>}
    </div>
  </li>)}
const StateToName = (state:GameState)=>{
  switch(state){
    case GameState.Practice: 
      return "Practice"
    case GameState.Lobby:
      return "Lobby"
    case GameState.Game:
      return "In Game"
    default:
      return "Unknown"
  }

}

export class PlayerList extends Component<PlayerListProps, PlayerListState>{
  render(props?: Readonly<PlayerListProps & Attributes & { children?: ComponentChildren; ref?: Ref<any>; }>, state?: Readonly<PlayerListState>, context?: any): ComponentChild {
    
    /*
    return ( <ul>
      { props.players.map(p=>( 
        <PlayerViewRow player={p} local={props.localUuid} 
          onclick={props.onChallenge}/>))}
    </ul> )
    */
   return ( <table class="table is-fullwidth">
    <thead>
      <tr>
        <th>Name</th>
        <th>Status</th>
        <th></th>
      </tr>
    </thead>
   <tbody>
    { props.players.map(p=>( 
      <PlayerViewRow player={p} local={props.localUuid} 
        onclick={props.onChallenge}/>))}
        </tbody></table> )

  }
}