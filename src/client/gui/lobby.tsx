import { h, Component,render, Attributes, ComponentChildren, ComponentChild, Ref } from "preact"
import { Comms } from "../../common/comms";
import { PlayerList } from "./playerlist";
import { IChallenge } from "../interfaces";
import { ChallengeOutListComp } from "./challengeoutlist";
import { ChallengeInList } from "./challengeinlist";
import { ChallengeStatus } from "../../common/enums";

export interface LobbyProps{
  message:string
  playerName:string
  lobby:Comms.LobbyDetails
  challengesIn:Array<IChallenge>
  challengesOut:Array<IChallenge>
  onRespond:(uuid:String, status:ChallengeStatus)=>void
  launchPractice:()=>void
  onChallenge:(uuid:string)=>void
  localUuid:string
}
export interface LobbyState{
  lobby:Comms.LobbyDetails
  message:string
  playerName:string
}


export class Lobby extends Component<LobbyProps, LobbyState>{
  render(props: Readonly<LobbyProps>, state:Readonly<LobbyState>): ComponentChild {
    return (
      <section class="section">
        <h1 class="title is-3">
        <span class=""> {state.playerName}</span>
        <span class="is-pulled-right">{props.lobby.name}</span>
     
        </h1>

        {state.message!= null ? (<div class="notification is-primary">
          <button class="delete" onClick={()=>{this.CloseMessage()}}></button>
          {state.message}
        </div>) : null}

        <div class="columns">
          <div class="column">
            
            <div class="box">
              <a class="button is-primary is-large" onClick={props.launchPractice}>Practice</a>
              &nbsp;
              <a class="button is-primary is-large" onClick={props.launchPractice}>Join Fight Queue</a>
              &nbsp;
              <a class="button is-primary is-large" onClick={props.launchPractice}>Switch Lobby</a>
            </div>

            <div class="box">
              <h2 class="title is-4">Challenges</h2>
              <ChallengeInList players={props.lobby.users} 
                challenges={props.challengesIn} 
                onRespond={props.onRespond }/>
            </div>

            <div class="box">
              <h2 class="title is-4">Challenges Issued</h2>
              <ChallengeOutListComp players={props.lobby.users} challenges={props.challengesOut}/>
            </div>

          </div>
          <div class="column">
            <div class="box">
              <h2 class="title is-4">Players</h2>
              <PlayerList players={props.lobby.users} onChallenge={props.onChallenge} localUuid={props.localUuid}/>
            </div>
          </div>
          
        </div>
      </section>)
  }
  
  CloseMessage(): any {
    this.setState({message:null})
  }

  constructor(props:LobbyProps){
    super(props)
    this.state = { 
      playerName: props.playerName, 
      lobby:props.lobby,
      message:props.message,
    }

  }



}