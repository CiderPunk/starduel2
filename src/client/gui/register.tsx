import { h, Component, ComponentChild, PreactDOMAttributes } from "preact"
import linkState from "linkstate"

export interface RegisterProps{
  onRegister:(name:string)=>void
  name:string
  message:string
}
export interface RegisterState{
  name:string
}

export class Register extends Component<RegisterProps>{
  render(props?: Readonly<RegisterProps>, state?: Readonly<RegisterState>): ComponentChild {
    const  message =  (props.message ? (<div class="notification is-primary">{props.message}</div>) : null)
    return (
      <section class="section">
        <h1 class="title is-1">Yo</h1>
        {message}
        <form>
          <div class="field">
            <label>Player name:</label><input type="text" class="input" value={state.name} onInput={ linkState(this, 'name')} />
          </div>
          <div class="field">
            <button class="button is-primary" type="submit" onClick={()=>{ props.onRegister( state.name )}}>Set Name</button>
          </div>
        </form>
      </section>
    )  
  }

  constructor(props: RegisterProps) {
    super(props);
    this.state = {  name:props.name };
  }
}