import { h, Component,render, Attributes, ComponentChildren, ComponentChild, Ref } from "preact"
import { Comms } from "../../common/comms";
import { ChallengeStatus } from "../../common/enums";
import { IChallenge } from "../interfaces";


interface ChallengeOutListProps{
  players:Array<Comms.UserDetails>
  challenges:Array<IChallenge>
}

interface ChallengeOutListState{
  challenges:Array<IChallenge>
}



interface ChallengeRowProp{
  player:Comms.UserDetails
  status:ChallengeStatus
  onRemove:(string)=>void
}



export class ChallengeOutListComp extends Component<ChallengeOutListProps, ChallengeOutListState>{

  render(props: Readonly<ChallengeOutListProps>, state: Readonly<ChallengeOutListState>): ComponentChild {
    //build list of challenges
    const challenges = state.challenges.map((c:IChallenge)=>{ 
      const player = props.players.find(p=>p.uuid == c.uuid) 
      return { player:player, status:c.status} as ChallengeRowProp
    })

    if (challenges.length > 0){
      return(
        <table class="table is-fullwidth">
          <thead>
            <tr>
              <th>Challenged</th>
              <th  width="20%">Status</th>  
              <th  width="20%"></th>
            </tr>
          </thead>
          <tbody>
            {challenges.map(c=>(<ChallengeOutView status={c.status} player={c.player} onRemove={(uuid:string)=>{ this.RemoveChallenge(uuid)}}/>))}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan={3} class="is-left">
                <a class="button is-small is-pulled-right" onClick={()=>{this.ClearAllChallenges()}}>Clear expired</a>
              </td>
            </tr>
          </tfoot>
        </table>
        
        )
    }
    else{
      return(<p>You have not issues any pending challenges</p>)

    }
  }

  ClearAllChallenges():void{
    this.setState({challenges:this.state.challenges.filter(c=>( c.status == ChallengeStatus.Pending) )})
  }

  RemoveChallenge(uuid:string){
    const i = this.state.challenges.findIndex(c=>(uuid == c.uuid))
    if (i>-1){
      this.state.challenges.splice(i)
      this.setState({challenges: this.state.challenges})
    }
  }


  constructor(props:any) {
    super(props);
    this.state = { 
      challenges: props.challenges     
    };
  }
}

const ChallengeOutView = (prop:ChallengeRowProp)=>( <tr>
    <th >{prop.player.name}</th>
    <td width="20%">{ChallengeStatusToString(prop.status)}</td>
    <td  width="20%">
      <a class="button is-small is-fullwidth is-disabled"
       disabled={prop.status == ChallengeStatus.Pending} onClick={()=>{prop.onRemove(prop.player.uuid)}}>Clear</a></td>
  </tr>)


const ChallengeStatusToString = (val:ChallengeStatus)=>{ switch (val){
  case ChallengeStatus.UserNotFound:
    return "User not found"
  case ChallengeStatus.Pending:
    return "Pending..."
  case ChallengeStatus.Blocked:
    return "Blocked"
  case ChallengeStatus.TimeOut:
    return "Expired"
  case ChallengeStatus.Declined:
    return "Declined"
  case ChallengeStatus.Disconnected:
    return "Disconnected"
  default:
    return "Unknown: " + val
}}