import { h, Component, ComponentChild } from "preact"
import { IClient } from "../interfaces";

export interface WaitingProps{
  message:string
}


export class Waiting extends Component<WaitingProps>{

  render(props?: Readonly<WaitingProps>): ComponentChild {
    return (
      <section class="section">
        <h1 class="title is-1">{props.message}</h1>
        <progress class="progress is-small is-primary" max="100">15%</progress>
      </section>
    )  
  }
}