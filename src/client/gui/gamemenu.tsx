import { h, Component, ComponentChild } from "preact"
import { Property } from "babylonjs-inspector";
export interface GameMenuProps{ show:boolean, returnToLobby:()=>void, returnToGame:()=>void }
export interface GameMenuState{}

export class  GameMenu extends Component<GameMenuProps, GameMenuState>{


  render(props?: Readonly<GameMenuProps>, 
    state?: Readonly<GameMenuState>, 
    context?: any): ComponentChild {
    return (
      <div class={props.show ? "modal is-active" : "modal "} >
      <div class="modal-background"></div>
      <div class="modal-content">

      <nav class="panel">
        <p class="panel-heading">
          Game Menu
          <button class="delete is-pulled-right"  onClick={props.returnToGame}></button>
        </p>
        <a class="panel-block" onClick={props.returnToGame}>
          Return to game
        </a>
        <a class="panel-block" onClick={props.returnToLobby}>
          Return to lobby
        </a>
      </nav>

      </div>
    </div>
    )
  }
 
}