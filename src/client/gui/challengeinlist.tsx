import { h, Component,render, Attributes, ComponentChildren, ComponentChild, Ref } from "preact"
import { Comms } from "../../common/comms";
import { ChallengeStatus } from "../../common/enums";
import { IChallenge } from "../interfaces";


export interface ChallengeInListProp{
  players:Array<Comms.UserDetails>
  challenges:Array<IChallenge>
  onRespond:(uuid:string, status:ChallengeStatus)=>void
}

export interface ChallengeInListState{
  challenges:Array<IChallenge>
}


interface ChallengeRowProp{
  player:Comms.UserDetails
  status:ChallengeStatus
  onRespond:(uuid:string, status:ChallengeStatus)=>void
}

export class ChallengeInList extends Component<ChallengeInListProp, ChallengeInListState>{
  render(props: Readonly<ChallengeInListProp>, state: Readonly<ChallengeInListState>): ComponentChild {
    const challenges = props.challenges.map((c:IChallenge)=>{ 
      const player = props.players.find(p=>p.uuid == c.uuid) 
      return { player:player, status:c.status} as ChallengeRowProp
    })
  if (challenges.length > 0){
    return(
      <table class="table is-fullwidth">
        <thead>
          <tr>
            <th>Challenger</th>
            <th  width="20%"></th>
          </tr>
        </thead>
        <tbody>
          {challenges.map(c=>(<ChallengeInView status={c.status} player={c.player} onRespond={props.onRespond} />))}
        </tbody>
      </table>)
    }
    else{
      return (<p>No incomming challenges</p>)
    }
}
constructor(props:ChallengeInListProp){
  super(props);
  this.state = { 
    challenges: props.challenges     
  };
}


}

/*

export const ChallengeInList = (props:ChallengeListProps)=>{
  const challenges = props.challenges.map((c:IChallenge)=>{ 
    const player = props.players.find(p=>p.uuid == c.uuid) 
    return { player:player, status:c.status} as ChallengeRowProp
  })

  if (challenges.length > 0){
  return(
    <table class="table is-fullwidth">
      <thead>
        <tr>
          <th>Challenger</th>
          <th  width="20%"></th>
        </tr>
      </thead>
      <tbody>
        {challenges.map(c=>(<ChallengeInView status={c.status} player={c.player} onRespond={props.onRespond} />))}
      </tbody>
    </table>)
  }
  else{

    return (<p>No pending challenges</p>)
  }
}
*/

//const ChallengeView = (prop:{ player:Comms.UserDetails})=>( <li >{prop.player.name}</li>)
const ChallengeInView = (prop:ChallengeRowProp)=>( <tr>
    <th >{prop.player.name}</th>
    <td width="50%">
      <div class="buttons field is-grouped is-pulled-right">
        <a class="button is-small" onClick={()=>{ prop.onRespond(prop.player.uuid, ChallengeStatus.Declined)}}>Decline</a>
        <a class="button is-small" onClick={()=>{ if (confirm("This will prevent further challenges from this user permanently")){ prop.onRespond(prop.player.uuid, ChallengeStatus.Blocked)}}}>Block</a>
        <a class="button is-small is-primary" onClick={()=>{ prop.onRespond(prop.player.uuid, ChallengeStatus.Accepted)}}>Accept</a>
      </div>
    </td>
    
  </tr>)
