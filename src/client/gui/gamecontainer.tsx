import { h, Component,render, Attributes, ComponentChildren, ComponentChild, Ref } from "preact"
import { IClient, IGame, IGameContainer, IGameComm } from "../interfaces";
import { Game } from "../game"
import { GameMenu } from "./gamemenu";
import { stat } from "fs";

export interface GameContainerProps{ 
  owner:IClient
  onQuitGame:()=>void
  gameComm?:IGameComm
}

interface GameContainerState{
  owner:IClient
  showMenu:boolean
  gameComm?:IGameComm
}

export class GameContainer extends Component<GameContainerProps, GameContainerState> implements IGameContainer{

  protected game:IGame

  render(props?: Readonly<GameContainerProps>,
     state?: Readonly<GameContainerState>, 
     context?: any): ComponentChild {
    return (
      <div class="game">
        <canvas id="renderCanvas"></canvas>    
        <canvas id="debugCanvas"></canvas>
        <GameMenu show={state.showMenu} 
          returnToLobby={props.onQuitGame} 
          returnToGame={()=>{ this.CloseMenu()}} />
      </div>
    )    
  }

  constructor(props: GameContainerProps) {
    super(props);
    this.state = { owner: props.owner , showMenu:false, gameComm:props.gameComm};
  }

  public CloseMenu():void{
    this.game.HideMenu()
    this.ToggleMenu(false)
  }

  public ToggleMenu(show:boolean):void{
    this.setState({ showMenu:show })
  }

  public  componentWillUnmount():void {
    //dispose of game safely
    this.game.Dispose()
    this.game = null
  }

  public componentDidMount():void {
    //init game
    this.game = new Game(this, this.state.gameComm)

  }
}