import { IGameComm, IClient } from "./interfaces";
import Peer = require('simple-peer');
import { Config } from "../server/config";
import { Client } from "./client";
import { EventNames, Comms } from "../common/comms";
import { Constants } from "./constants";
 


export class GameComm implements IGameComm{

  protected readonly pingsSent = new Array<number>()
  protected readonly pongsRcvd = new Array<number>()

  protected p:Peer.Instance


  constructor(protected readonly client:IClient, initiator:boolean){
    this.p = new Peer({ 
      initiator: initiator, 
      trickle:false, 
      config:Config.iceConfig
    })
    this.p.on("error", (err)=>{
      console.log("peer error", err)
      this.client.AbortGame("Error connecting")
    })
    this.p.on("signal", (data)=>{     
      console.log("Sending signal data...")
      this.client.SendCommData(data) 
    })
    this.p.on("connect", ()=>{ this.Connected()})
    this.p.on(EventNames.Ping, (req:Comms.Ping)=>{
      req.responseTime = performance.now()
      this.p.emit(EventNames.Pong, req)
    })
    this.p.on(EventNames.Pong, (req:Comms.Ping)=>{ this.ReceivePong(req)  })
  }

  Connected(){
    this.client.SetWait("Testing connection...")
    console.log("Peer connected...")
    var promise = this.PingTest()
    promise.then((avgPing:number)=>{ 
      if (avgPing > Constants.MaximumLatency){
        this.client.AbortGame("Latency too high")

      }
      this.client.StartGame(this) })
  }

  PingTest(quantity:number = 30, delay:number = 10, wait:number=1000, outliers:number =0.1):Promise<number>{
    //clear pending pings...
    this.pingsSent.length = 0
    this.pongsRcvd.length = 0
    let toSend = quantity
    const timer = setInterval(()=>{
      this.SendPing(); 
      if (--toSend <= 0){
        clearInterval(timer)
      }
    }, delay)
    return new Promise((resolve)=>{
      setTimeout(()=>resolve(this.AnalysePingTest(quantity, outliers)) , wait)
    })
  }

  AnalysePingTest(quantity:number, outliers:number): number {
    const noReply = this.pingsSent.length
    if (noReply < quantity){

      /*
      let min = 9999
      let max = 0
      const total = this.pongsRcvd.reduce((acc, cv)=>{ 
        min = min < cv ? min : cv
        max = max > cv ? max : cv
        return acc+cv
      })
      */
      
      //skip outliers at the top and bottom
      let len = this.pongsRcvd.length
      let skip = Math.floor(outliers * len)
      const sorted =  this.pongsRcvd.sort()
      const min = sorted[0]
      const max = sorted[sorted.length-1]
      const normalised = sorted.splice(skip, len - (2 * skip))
      const total = normalised.reduce((acc, cv)=>{ return acc+cv })
      const avg = total / normalised.length
      console.log(`Ping report - pings:${quantity}, lost:${noReply}, min:${min}, max:${max}, normavg:${avg}`)
      return avg;
    }
    else{
      console.log(`Ping report - pings:${quantity}, lost:${noReply}`)
      return 999;
    }
  }


  
  ReceivePong(req:Comms.Ping){  
    const arrivalTime = performance.now()
    const i = this.pingsSent.indexOf(req.requestTime)
    if (i > -1){
      //this is a recent ping and should be considered.... maybe
      this.pongsRcvd.push(arrivalTime - req.requestTime)
      //remove index
      this.pingsSent.splice(i)
    }
    

  }
  

  SendPing(){
    const t = performance.now()
    this.pingsSent.push(t)
    this.p.emit(EventNames.Ping, {requestTime:t } as Comms.Ping)
  }

  Signal(data: any) {
    console.log("Recieved signal data...")
    this.p.signal(data)
  }
}