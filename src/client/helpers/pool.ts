import { FastArray } from "./fastarray";
import { IGame } from "../interfaces";

export interface IPool{
  Release(item:any)

}

export interface IPooledItem<T>{
  //constructor(name:string, owner:IGame, pool:IPool)
  Free:()=>void
  CleanUp:()=>void
}

export abstract class PooledItem{
  pool:IPool = null

  CleanUp:()=>void

  Free(): void {
    this.CleanUp()
    this.pool.Release(this)
  }
}


export class Pool<T extends IPooledItem<T>> implements IPool{
  protected pool:FastArray<T>
  protected count = 1
  protected readonly options:any

  constructor( protected owner:IGame, protected namePrefix:string,
    protected getNewItem:(name:string, owner:IGame, pool:IPool, options:any)=>T, 
    protected customOptions:any = null, defaultOptions:any = null){
    this.options = { ...defaultOptions, ...customOptions}
    this.pool = new FastArray<T>();
  }

  public Release(item:T){
    this.pool.push(item);
  }

  public GetNew():T{
    let res:T
    if (!(res = this.pool.pop())){
      return this.getNewItem(this.namePrefix+(this.count++), this.owner, this, this.options);
    }
    return res
  }


  public BulkPopulate(count:number){
    var preloadOpts = { preload:true, ...this.options} 
    for (let i = 0; i < count; i++){
      const item = this.getNewItem(this.namePrefix+(this.count++), this.owner, this, preloadOpts)
      this.Release(item)
    }
  }

}