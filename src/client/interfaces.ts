import * as BABYLON from 'babylonjs'
import { GameState, ChallengeStatus } from '../common/enums';
import { ParticleType } from './constants';

export interface IAccount{
  name:string
  hash:string
  uuid:string
  lastLobby:string
  blockList:Array<string>
  AddBlock(string):void
  RemoveBlock(string):void

}

export interface ILoader{
  preload:(assetManager:BABYLON.AssetsManager)=>void
}

export interface IClient{
  StartGame(gc:IGameComm): any;
  AbortGame(message: string):void;
  SendCommData(data: any): void
  SetWait(message:string):void
  //RegisterUser(name: string): void
}


export interface IGameContainer{
  ToggleMenu(show:boolean)
}

export interface IGame{
  Dispose():void
  HideMenu():void
  scene:BABYLON.Scene
  world:IWorld
  entman:IEntityMan
  effectsman:IEffectsMan
  bulletPool:IBulletPool
  splosionPool:ISplosionPool
}


export interface IGameComm{
  Signal(data: any): void;
}

export interface IEffectsMan{
  PreDraw:(delta:number)=>void
  RegisterEffect:(target:IEffect)=>void
}

export interface IEffect{
  PreDraw:(delta:number)=>boolean
}

export interface IChallenge{
  uuid:string
  status:ChallengeStatus
}

export interface IEntity{
  PrePhysics:()=>boolean
  PreDraw:()=>void
  meshY:()=>number
  meshX:()=>number
  
  mass:() =>number
  pos:()=>IV2 
  vel:()=>IV2 
  ang:() =>number 
  Hurt:(inflictor:IEntity, energy:number, name:string )=>void

  CaclulateGravity(target:IEntity):void
  AddForce:(force:IV2)=>void
  //mass:()=>number
  body:planck.Body
  Collision(other:IEntity):void
  owner:IGame
  name:string
}

export interface IWorld{
  Dispose():void
  world:planck.World
  Step(time:number):void
  Init():void
}

export interface IV2 {
  x:number
  y:number
}

export interface IEntityMan{
  PrePhysics():void
  PreDraw():void
  RegisterEntity(target:IEntity, heavy:boolean):void
}

export interface IBullet{
  Init(pos:IV2, vel:IV2, density:number, shooter:IShooter)
}

export interface IBulletPool{
  GetNew():IBullet
}

export interface ISplosion{
  Init(emitter:IEntity, emitRate?:number, type?:ParticleType)
}

export interface ISplosionPool{
  GetNew():ISplosion
}


export interface IWeapon{
  Update:(firing: boolean)=>void
  SetOwner:(ent:IShooter)=>void
}
export interface IShooter extends IEntity{ 
  groupId:number
}
