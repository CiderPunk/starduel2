import { IWorld, IGame, IEntity } from "../interfaces";
import { BulletPool } from "../ents/bullet";

export abstract class BaseWorld implements IWorld{

  Init(): void {
    throw new Error("Method not implemented.");
  }

  public world:planck.World

  protected owner:IGame

  public Dispose():void{ }
  
  public constructor(owner:IGame){
    this.owner = owner;
    //create box2d world
    this.world = new planck.World();
    this.world.on("begin-contact",this.Contact);
  }

  public Step(time:number):void{
    this.world.step(time)
  }

  protected Contact(contact: planck.Contact): void {
    var ent1 = contact.getFixtureA().getBody().getUserData() as IEntity;
    var ent2 = contact.getFixtureB().getBody().getUserData() as IEntity;
    if (ent1 && ent2){
      //TODO: check for bullets....
      ent1.Collision(ent2)
      ent2.Collision(ent1)
    }
  }


}