import { BaseWorld } from "./baseworld";
import { Spec } from "../../common/gamespec";
import { IGame, IEntity } from "../interfaces";
import { CircleWorld, CircleWorldConst } from "./circleworld";
import { CircleBoundary } from "../ents/circleboundary";
import { Asteroid } from "../ents/asteroid";
import { V2 } from "../helpers/v2";
import { Star, StarConsts } from "../ents/star";

export class NetWorld extends CircleWorld{
  constructor (owner:IGame, protected spec:Spec.GameSpec){
    super(owner)
  }

  public init(){
    this.boundary = new CircleBoundary(this.owner, this.spec.radius, CircleWorldConst.Segments, CircleWorldConst.DisplaySegments )
    //create some roids to prevent excess creation during the game
    this.mediumAsteroidPool.BulkPopulate(50)
    this.smallAsteroidPool.BulkPopulate(100)

    this.spec.ents.forEach((e=>{

      switch(e.entType){
        case Spec.EntTypeNames.Asteroid:{
          this.SpawnRoid(e)
          break;
        }
        case Spec.EntTypeNames.Player:{
          this.SpawnPlayer(e)
          break;
        }
        case Spec.EntTypeNames.Star:{
          this.SpawnStar(e)         
        }
        default:
        //do nothing
          

      }
    }))
  }

  SpawnStar(def: Spec.EntSpec): void {
    var starspec = def.data as Spec.StarSpec

    new Star("star2", this.owner,new V2().setV2(def.pos),
      starspec.radius, starspec.density, starspec.intensity,
      this.GetStarColours(starspec.colour) ,new V2().setV2(def.vel), def.av)
  }

  GetStarColours(name:string):BABYLON.Color3[]{
    switch (name){
      case Spec.StarColours.Acid:
        return BABYLON.FireProceduralTexture.GreenFireColors
      case Spec.StarColours.Purple:
        return BABYLON.FireProceduralTexture.PurpleFireColors
      case Spec.StarColours.Blue:
        return BABYLON.FireProceduralTexture.BlueFireColors
      default:
        return StarConsts.firecols
    }
  }

  SpawnPlayer(e: Spec.EntSpec): void {
    throw new Error("Method not implemented.");
  }

  protected SpawnRoid(spec:Spec.EntSpec){
    var roid:Asteroid
    switch(spec.entSubType){
      case Spec.Size.Large:
        roid = this.largeAsteroidPool.GetNew()
        break
      case Spec.Size.Medium:
        roid = this.mediumAsteroidPool.GetNew()
        break
      default:
        roid = this.smallAsteroidPool.GetNew()
        break
    }
    roid.Init(new V2().setV2(spec.pos), new V2().setV2(spec.vel),spec.av)
  }

}