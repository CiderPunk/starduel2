import { BaseWorld } from "./baseworld";
import { IGame, IEntity } from "../interfaces";
import { V2 } from "../helpers/v2";
import { Star, StarConsts} from "../ents/star";
import { AsteroidPool } from "../ents/asteroid";
import { CircleBoundary } from "../ents/circleboundary";
import { Constants } from "../constants";
import { CubeMapToSphericalPolynomialTools } from "babylonjs";
import { spawn } from "child_process";

export namespace CircleWorldConst{
  export const Radius = 500
  export const Segments = 32
  export const DisplaySegments = 128
}


export class CircleWorld extends BaseWorld{

  protected largeAsteroidPool:AsteroidPool
  protected mediumAsteroidPool:AsteroidPool
  protected smallAsteroidPool:AsteroidPool

  protected boundary:CircleBoundary

  public constructor (owner:IGame){
    super(owner)
    this.largeAsteroidPool = new AsteroidPool(owner, "Asteroid_lg_", { scale:2, spawnInvulnerabilityTime:0, onDeath: (roid:IEntity)=>{
      this.SpreadRoids(roid, this.mediumAsteroidPool)
    }})
    this.mediumAsteroidPool = new AsteroidPool(owner, "Asteroid_md_",{ scale:1.6, spawnInvulnerabilityTime:0.5, onDeath: (roid:IEntity)=>{
      this.SpreadRoids(roid, this.smallAsteroidPool)
    }})
    this.smallAsteroidPool = new AsteroidPool(owner, "Asteroid_sm_",{ scale:1, spawnInvulnerabilityTime:0.5})
  }

  static readonly vel:V2 = new V2()
  static readonly spreadVects:Array<V2> = [new V2(), new V2()]

  protected SpreadRoids(parent:IEntity, pool:AsteroidPool){
    CircleWorld.vel.setV2(parent.vel()).mul(1 + Math.random() * 0.2).fanArray(0.2 * Math.PI, CircleWorld.spreadVects)
    CircleWorld.spreadVects.forEach((v:V2)=>{
      const baby = pool.GetNew()
      baby.Init( parent.pos() as V2, v, Math.random()* 1)
    })
  }

  public Init(){ 


    //create some roids to prevent excess creation during the game
    this.mediumAsteroidPool.BulkPopulate(50)
    this.smallAsteroidPool.BulkPopulate(100)

    //boundary 
    this.boundary = new CircleBoundary(this.owner, CircleWorldConst.Radius, CircleWorldConst.Segments, CircleWorldConst.DisplaySegments )

    //add star
/*
    //binaries
    new Star("star1", this.owner,new V2(50,0), 20, 50,50000, BABYLON.FireProceduralTexture.GreenFireColors ,new V2(0,15),-3)
    new Star("star2", this.owner,new V2(-50,0), 20, 50, 50000, BABYLON.FireProceduralTexture.PurpleFireColors ,new V2(0,-15))
 */

    //unbalanced binaries
    var mainstar = new Star("star1", this.owner,new V2(0,0), 30, 100, 50000, StarConsts.firecols ,new V2(0,2))
    new Star("star2", this.owner,new V2(-200,0), 10, 70, 20000, StarConsts.firecols ,new V2(0,-34))

/*
  //boring one star
  var mainstar = new Star("star1", this.owner,new V2(0,0), 30, 100, 50000, StarConsts.firecols ,new V2(0,1.5))
  */
  
  /*
    //three star system
    new Star("star1", this.owner,new V2(0,0), 40, 50, 20000, StarConsts.firecols ,new V2(0,0))
    new Star("star2", this.owner,new V2(-200,0), 20, 50, 20000, StarConsts.firecols ,new V2(0,-35))
    new Star("star3", this.owner,new V2(200,0), 20, 50, 20000, StarConsts.firecols ,new V2(0,35))
  */

    var centralMass = mainstar.mass()

    //asteroids
    for (var i = 0; i < 50; i++){
      var orbitRadius = 100+ (Math.random() * 400)
      const pos = new V2(0,0).setAngle(Math.random() * 2 * Math.PI, orbitRadius )
      var targetSpeed = Math.sqrt(( centralMass * Constants.GravityConstant) / orbitRadius)
      const vel = new V2(pos.x,pos.y).norm().rotateCW().mul(targetSpeed + ((Math.random() - 0.5 )* 10) )
      const spin =  (Math.random() * 5) - 2.5
      var roid = this.largeAsteroidPool.GetNew()
      roid.Init(pos,vel,spin)
      //new Asteroid("asteroid" + i, this.owner, pos, vel,)
    }

  }



}