import { IEntityMan, IEffect, IEffectsMan } from "./interfaces";
import { FastArray } from "./helpers/fastarray";

export class EffectsMan implements IEffectsMan{
  
  protected effects = new FastArray<IEffect>()
  protected scratch = new FastArray<IEffect>()

  public RegisterEffect(target:IEffect):void{
    this.effects.push(target)
  }

  public PreDraw(delta:number): void {
    var eff:IEffect
    while(eff = this.effects.pop()){
      if (eff.PreDraw(delta)){
        this.scratch.push(eff)
      }
    }
    this.effects.swap(this.scratch)
  }

  
}