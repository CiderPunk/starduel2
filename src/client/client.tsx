import { h, Component, ComponentChild } from "preact"
import { IClient, IGame, IAccount, IChallenge, IGameComm } from "./interfaces";
import * as io from "socket.io-client"
import { Game } from "./game"
import { GameState, JoinResult, ChallengeStatus } from "../common/enums"
import { Account } from "./user/account"
import { EventNames, Comms } from "../common/comms"
import { Waiting } from "./gui/waiting"
import { Register } from "./gui/register"
import { GameContainer } from "./gui/gamecontainer";
import { ControlMan } from "./controlman";
import { Lobby } from "./gui/lobby";
import { GameComm } from "./gamecomm";


export interface ClientProps{

}

interface ClientState{
  gameState:GameState
  message?:string
  lobby?:Comms.LobbyDetails
  localUuid:string
  challengesOut:Array<IChallenge>
  challengesIn:Array<IChallenge>
  playerName:string
}

export class Client extends Component<ClientProps, ClientState> implements IClient{


  game:IGame
  public readonly account:IAccount
  public readonly socket:SocketIOClient.Socket
  protected gc:IGameComm


  RegisterUser(name: string): void {
    this.setState({ gameState:GameState.Wait, message:"Registering account..."});
    //tell server
    this.socket.emit(EventNames.RegisterRequest, { name: name })
  }

  render(props:Readonly<ClientProps>,
      state?:Readonly<ClientState>): ComponentChild {
    switch (this.state.gameState){
      case GameState.Wait:
        return ( <Waiting message={state.message}/> )
      case GameState.Register:
        return ( <Register onRegister={(name:string)=>{this.RegisterUser(name)}} message={state.message} name={this.account.name}/> )
      case GameState.Game:
        return ( <GameContainer owner={this} onQuitGame={()=>{ this.LeaveGame() }} gameComm={this.gc}/> )
      case GameState.Practice:
        return ( <GameContainer owner={this} onQuitGame={()=>{ this.LeaveGame() }}/> )
      case GameState.Lobby:
        return ( <Lobby playerName={state.playerName}  lobby={state.lobby} 
          message={state.message}
          localUuid={state.localUuid}
          launchPractice={()=>{ this.LaunchPractise()}}
          onChallenge={(uuid:string)=>{ this.SendChallenge(uuid)} }
          challengesIn={state.challengesIn} 
          challengesOut={state.challengesOut}
          onRespond={(uuid:string, status:ChallengeStatus)=>{ this.ChallengedRespond(uuid, status)}}
          />)
    }  
  }




  ChallengedRespond(uuid: string, status: ChallengeStatus): void {

    if (status== ChallengeStatus.Accepted){
      //set up new game start webrtc crap
    }
    else{
      //remove challenge from list
      const i = this.state.challengesIn.findIndex((s)=>(s.uuid == uuid))
      if (i > -1){
        this.state.challengesIn.splice(i,1)
        this.setState({challengesIn:this.state.challengesIn})
      }
      if (status == ChallengeStatus.Blocked){
        //add user to block list
        this.account.AddBlock(uuid)
      }
    }
    //send to server
    this.socket.emit(EventNames.ChallengeResponse, {uuid:uuid, status:status} as Comms.Challenge)
  }


  constructor(props:any) {
    super(props);

    
    //do stuff yeah
    ControlMan.init();
    this.account = new Account()
    this.socket = io()

    this.socket.on("connect", ()=>{ 
      console.log("Connected")
      this.JoinRequest() 
    })
    this.socket.on("disconnect", ()=>{ console.log("Disconnected") })
    this.socket.on(EventNames.JoinResponse,  (resp:Comms.JoinResponse )=>{ this.JoinResponse(resp) })
    this.socket.on(EventNames.RegisterResponse,  (resp:Comms.RegisterResponse )=>{this.RegResponse(resp)})
    this.socket.on(EventNames.PushedLobby, (lobby:Comms.LobbyDetails)=>{this.JoinLobby(lobby)})
    this.socket.on(EventNames.LobbyUpdate, (player:Comms.LobbyDetails)=>{ this.UpdateLobby(player) })
    this.socket.on(EventNames.Challenged, (challenge:Comms.Challenge)=>{ this.Challenged(challenge) })
    this.socket.on(EventNames.ChallengeResponse, (challenge:Comms.Challenge)=>{ this.ChallengeResponse(challenge) })
    this.socket.on(EventNames.InitGC, (req:Comms.InitGameComm)=>{this.InitGameComm(req.initiator)})
    this.socket.on(EventNames.SignalGC, (req:Comms.SignalGameComm)=>{this.gc.Signal(req.data)})

    this.state = { 
      gameState: GameState.Wait,
      message:"Connecting...", 
      localUuid:"",
      challengesOut: new Array<IChallenge>(),
      challengesIn: new Array<IChallenge>(),
      playerName:this.account.name,
    };

  }

  RegResponse(resp:Comms.RegisterResponse):void{
    this.account.name = resp.name
    this.account.uuid = resp.uuid
    this.account.hash = resp.hash
    this.JoinRequest()
  }

  JoinRequest():void{
    this.socket.emit(EventNames.JoinRequest, {
      name: this.account.name, 
      uuid: this.account.uuid,
      hash: this.account.hash,
      lobbyId: this.account.lastLobby,
     } as Comms.JoinRequest)
  }

  JoinResponse(resp:Comms.JoinResponse){
    switch(resp.result){
      case JoinResult.AlreadyConnected:
        this.setState({ gameState:GameState.Wait, message:resp.message})
        break
      case JoinResult.AuthError:
      case JoinResult.NoAccount:
        this.setState({ gameState:GameState.Register, message:resp.message  })
        break
    }
  }

  LeaveGame(){
    this.setState({ gameState: GameState.Lobby })
    this.socket.emit(EventNames.StatusUpdate, { state: GameState.Lobby } as Comms.ChangeStatus)
  }

  JoinLobby(lobby:Comms.LobbyDetails):void{
    this.setState({ gameState:GameState.Lobby, lobby: lobby, message:null,  localUuid:this.account.uuid })
  }

  UpdateLobby(lobby:Comms.LobbyDetails):void{
    this.setState({ lobby: lobby })
  }

  LaunchPractise(){
    this.setState({ gameState: GameState.Practice })
    this.socket.emit(EventNames.StatusUpdate, { state: GameState.Practice } as Comms.ChangeStatus)
  }

  SendChallenge(uuid:string){
    this.socket.emit( EventNames.ChallengeRequest, { uuid:uuid  } as Comms.Challenge)
    //check for existing challenge
    let c = this.state.challengesOut.find((c)=>(c.uuid == uuid))
    if (c!= null){
      c.status = ChallengeStatus.Pending
    }
    else{
      this.state.challengesOut.push({uuid:uuid, status:ChallengeStatus.Pending} as IChallenge)
    }
    //send it...
    this.setState({ challengesOut:this.state.challengesOut })
  }

  Challenged(challenge: Comms.Challenge): void {
    this.state.challengesIn.push({ uuid:challenge.uuid, status:ChallengeStatus.Pending })
    this.setState({ challengesIn:this.state.challengesIn })
  }

  ChallengeResponse(cng:Comms.Challenge): any {
    const challenge = this.state.challengesOut.find(c=>(cng.uuid == c.uuid))
    if (challenge!= null){
      challenge.status = cng.status
      this.setState({ challengesOut:this.state.challengesOut })
    }
  }

  InitGameComm(initiator: boolean): void {
    this.SetWait("Establishing connection...")
    this.gc= new GameComm(this, initiator)
  }
  SendCommData(data: any): void {
    this.socket.emit(EventNames.SignalGC, { data:data } as Comms.SignalGameComm)
  }

  SetWait(msg:string){
    this.setState({gameState:GameState.Wait, message:msg})
  }
  AbortGame(msg: string): void {
    this.setState({ gameState: GameState.Lobby, message:msg })
    this.socket.emit(EventNames.StatusUpdate, { state: GameState.Lobby } as Comms.ChangeStatus)
  }
  StartGame(gc:IGameComm) {
    this.AbortGame("Testing!")
    //throw new Error("Method not implemented.");
  }

}