import { IAccount } from "../interfaces";

/**
 * persists user account details
 */
export class Account implements IAccount{

  public get name():string{ return localStorage.getItem("name") }
  public set name(val:string){  localStorage.setItem("name", val ) }
  public get uuid():string{ return localStorage.getItem("uuid") }
  public set uuid(val:string){ localStorage.setItem("uuid", val) }
  public get hash():string{ return localStorage.getItem("hash") }
  public set hash(val:string){ localStorage.setItem("hash", val) }
  public get lastLobby():string{ return localStorage.getItem("lastLobby") }
  public set lastLobby(val:string){ localStorage.setItem("lastLobby", val ) }

  public get blockList():Array<string>{   
    return localStorage.getItem("blockList") .split(',')
  }

  public AddBlock(uuid:string):void{
    var list = this.blockList
    list.push(uuid)
    localStorage.setItem("blocklist", list.join(","))
  }

  public RemoveBlock(uuid:string):void{
    var list = this.blockList
    const i = list.findIndex((b)=>(b == uuid))
    if (i > -1){
      list.splice(i,1)  
      localStorage.setItem("blocklist", list.join(","))
    }  
  }

}