export namespace Constants{
  export const GravityConstant =1
  export const TickTime = 1/120 //120 / second 

  export const MaximumLatency = 500


}


export enum ParticleType{
  Rubble,
  Flare,

}


export enum CollisionGroup{
  boundary = 0x0001,
  asteroid = 0x0002,
  star = 0x0004,
  ship = 0x0008,
  projectile = 0x0010,
  all = 0xffff,
}
