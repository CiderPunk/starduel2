import { IGame, IEntity, ILoader, ISplosion, IEffect } from "../interfaces";
import { IPooledItem, Pool, IPool, PooledItem } from "../helpers/pool";
import { applyMixins } from "../helpers/mixins";
import { V2 } from "../helpers/v2";
import { ParticleType } from "../constants";

export class SplosionPool extends Pool<Splosion> implements IPool{

  constructor(owner:IGame){
    super(owner, "splosion_", Splosion.GetNew);
  }

  Release(item: any) {
    this.pool.push(item)
  }
}

export class Splosion implements IPooledItem<Splosion>, PooledItem, ISplosion, IEffect{





  protected ps:BABYLON.ParticleSystem
  readonly loc:BABYLON.Vector3
  private static readonly  v1 = new V2()
  private static readonly  v2 = new V2()
  private static readonly scratchVelocity:V2 = new V2()
  protected emiting:boolean
  protected lifeTime:number
  static particleTex:BABYLON.Texture
  static rubbleTex:BABYLON.Texture

  pool: IPool;
  Free: () => void;
  CleanUp(){
  }


  protected constructor(protected name:string, protected owner:IGame, pool:IPool, protected readonly options:any = null){
    this.pool = pool
    this.loc = new BABYLON.Vector3()
    //this.ps = new BABYLON.GPUParticleSystem("splosion",{ capacity:300, randomTextureSize:256 }, this.owner.scene)
    this.ps = new BABYLON.ParticleSystem("splosion",300, this.owner.scene)
    this.ps.particleTexture = Splosion.particleTex
    this.ps.emitRate = 100
    this.ps.createDirectedSphereEmitter(4, new BABYLON.Vector3(), new BABYLON.Vector3())
  }


  public static GetNew(name:string, owner:IGame, pool:IPool, options:any = null):Splosion{
    return new Splosion(name,  owner, pool, options)
  }


  PreDraw(delta:number):boolean{
    this.lifeTime+=delta

    if (this.lifeTime > 200 && this.emiting){
      this.ps.stop()
      this.emiting = false
    }

    if (this.lifeTime >1500){
      this.ps.reset()
      //this.ps.activeParticleCount = 0
      this.Free()
      return false
    }
    return true
  }



  public static loader():ILoader {
    return{
      preload: (assMan:BABYLON.AssetsManager)=>{
        assMan.addTextureTask("splosions","assets/flare2.png").onSuccess = (task)=>{
          Splosion.particleTex = task.texture
        }
        assMan.addTextureTask("rubble","assets/rubble.png").onSuccess = (task)=>{
          Splosion.rubbleTex = task.texture
        }
      } 
    }
  }


  Init(emitter:IEntity, emitRate:number = 300, type:ParticleType = ParticleType.Flare ){
    console.log(`${this.name} initialized`)
    switch(type){
      case ParticleType.Flare:
        this.ps.particleTexture = Splosion.particleTex
        this.ps.minSize = 0.5
        this.ps.maxSize = 1
        break

      case ParticleType.Rubble:
        this.ps.particleTexture = Splosion.rubbleTex
        this.ps.maxSize = 3
        this.ps.minSize = 1.5
        break
    }


    var vel = Splosion.scratchVelocity
    var point = emitter.pos()
    vel.setV2(emitter.vel())
    this.ps.emitter = this.loc.set(point.x,point.y,0)


    
    //this.ps.activeParticleCount = emitRate
    this.ps.emitRate = emitRate
    this.ps.start()
    this.owner.effectsman.RegisterEffect(this)
    this.emiting = true
    this.lifeTime = 0
    vel.fan(0.2 * Math.PI,Splosion.v1, Splosion.v2)
    this.ps.direction1.set(Splosion.v1.x, Splosion.v1.y, -1)
    this.ps.direction2.set(Splosion.v2.x, Splosion.v2.y, 1)
    //this.ps.direction1.set(this.v2.x, this.v2.y, -10)
/*
    var t = setTimeout(()=>{
      this.ps.stop()
      setTimeout(()=>{
      
        this.Free() 
      },3000)
    }, 200)
    */
  }



}

applyMixins(Splosion,[PooledItem])