import * as BABYLON from 'babylonjs'
import 'babylonjs-loaders'
import 'babylonjs-materials'
import 'babylonjs-procedural-textures'
import 'babylonjs-inspector';
import 'planck-js'
import { ILoader, IGame, IEntity, IWorld, IEntityMan, IEffectsMan, IGameContainer, IGameComm } from './interfaces'
import { Ship } from './ents/ship'
import { ControlMan, Command } from './controlman'
import { DebugRenderer } from './debugrenderer'
import { CircleWorld } from './world/circleworld';
import { EntityMan } from './entman';
import { V2 } from './helpers/v2';
import { Asteroid, AsteroidPool } from './ents/asteroid';
import { Constants } from './constants';
import { BulletPool } from './ents/bullet';
import { LocalPlayer } from './ents/localplayer';
import { Splosion, SplosionPool } from './effects/splosion';
import { EffectsMan } from './effectsman';


namespace GlobalControls{
  
  export const EnableDebug =  new Command("ENABLE_DEBUG","DEBUG",113)
  export const EnableInspector =  new Command("ENABLE_INSPECTOR","INSPECT",115)
  export const ToggleMenu =  new Command("TOGGLE_MENU","MENU",27)

  ControlMan.registerCommands([ EnableDebug, EnableInspector, ToggleMenu ]);
}

export class Game  implements IGame{
  protected canvas:HTMLCanvasElement
  protected engine:BABYLON.Engine
  public scene:BABYLON.Scene
  public world:IWorld
  public readonly entman:IEntityMan
  public readonly effectsman:IEffectsMan
  protected container:BABYLON.AssetContainer
  protected camera:BABYLON.FreeCamera
  protected lastTime:number = 0
  protected readonly tickTimeMs = Constants.TickTime * 1000
  protected tickTime:number = 0
  protected followTarget:IEntity
  protected debug:DebugRenderer
  bulletPool:BulletPool
  splosionPool:SplosionPool
  protected debugView:boolean = false
  protected inspectorView:boolean = false
  protected menuView:boolean = false

  public SetVisible(show:boolean){
    this.canvas.style.display = show ? "block" : "none"
   }

  Dispose(): void {
    this.world.Dispose()
    this.scene.dispose()
  } 
  
  protected onLoad(){
    this.container.moveAllFromScene()
    this.world = new CircleWorld(this)
    this.world.Init()
    //var camera = new BABYLON.ArcRotateCamera("Camera", Math.PI / 2, Math.PI / 2, 2, BABYLON.Vector3.Zero(), scene)
    this.camera = new BABYLON.FreeCamera("Camera", new BABYLON.Vector3(0,0,350), this.scene)
    this.camera.setTarget(new BABYLON.Vector3(0,0,0))
    this.camera.rotation.set(0,1 * Math.PI,Math.PI)
    var ship1  = new LocalPlayer("player1", this, new V2(0,150), new V2(30,0))  
    this.followTarget = ship1;
    this.debug = new DebugRenderer(this.world.world)


    //bullet pool, where the bullets come from!
    this.bulletPool = new BulletPool(this);
    //this.bulletPool.BulkPopulate(50)
    this.splosionPool = new SplosionPool(this)  
  
    //initialize some explosions
    this.splosionPool.BulkPopulate(100)
    this.bulletPool.BulkPopulate(50)
    window.requestAnimationFrame((t:number)=>{ this.update(t) })
  }




  protected update(time:number):void{
    //update timing
    var delta = time - this.lastTime
    this.lastTime = time

    window.requestAnimationFrame((t:number)=>{ this.update(t) })
    this.PreDraw(delta)
    this.engine.beginFrame()
    this.scene.render()
    this.engine.endFrame()
    if (this.debugView){
      //debug draw!
      this.debug.render()
    }

    if (delta > 500){
      this.tickTime = time
      console.log("frames missed: " + delta) 
    }
    while(this.tickTime < time){
      //console.log("tick\n");
      this.entman.PrePhysics()
      this.world.Step(Constants.TickTime)
      this.tickTime += this.tickTimeMs;
    }
  }


  public PreDraw(delta:number) {
    var targetpos = this.followTarget.pos()
    this.camera.position.set(targetpos.x, targetpos.y,400)
    //update cam position
    this.camera.position.x = this.followTarget.meshX()
    this.camera.position.y = this.followTarget.meshY()
    this.entman.PreDraw()
    this.effectsman.PreDraw(delta)
  }


  public HideMenu(){
    this.menuView = false
  }

  public constructor(protected readonly gameContainer:IGameContainer, protected readonly gc:IGameComm ){
    //do stuff yeah
//    ControlMan.init();
    this.canvas = document.getElementById("renderCanvas") as HTMLCanvasElement;
    this.SetVisible(true)

    this.engine = new BABYLON.Engine(this.canvas, true);
    this.engine.enableOfflineSupport = false;

    this.scene = new BABYLON.Scene(this.engine)
    this.scene
    var assMan = new BABYLON.AssetsManager(this.scene)
    var loaders = new Array<ILoader>()

    this.entman = new EntityMan()
    this.effectsman = new EffectsMan()
    //asset container
    this.container = new BABYLON.AssetContainer(this.scene)

    //crrate list of asset loaders...
    loaders.push(Ship.loader(), Asteroid.loader(), Splosion.loader())

    // run loaders preloads 
    loaders.forEach((loader)=>{
      loader.preload(assMan)
    })
    assMan.onFinish = ()=>{this.onLoad();}
    assMan.load();
  
    GlobalControls.EnableDebug.Subscribe((active, name) =>{
      if(active){
        this.debugView = !this.debugView
        this.debug.setVisible(this.debugView)
      }
    })
    GlobalControls.EnableInspector.Subscribe((active, name) =>{
      if(active){
        this.inspectorView = !this.inspectorView
        this.inspectorView ? this.scene.debugLayer.show() : this.scene.debugLayer.hide()
      }
    })
    GlobalControls.ToggleMenu.Subscribe((active,name)=>{ 
      if(active){
        this.menuView = !this.menuView
        this.gameContainer.ToggleMenu(this.menuView)
      }
    })
  }



 



} 