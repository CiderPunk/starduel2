import { IEntity, IV2, IGame } from "../interfaces";
import { V2 } from "../helpers/v2";
import { Constants } from "../constants";
import { MeshBuilder, Material } from "babylonjs";
import { CollisionGroup } from "../constants";


/**
 * dummy entity for the world boundary collisions
 */
export class CircleBoundary implements IEntity{

  mesh:BABYLON.Mesh
  body: planck.Body;
  public static  Zero:V2 = new V2(0,0)

  Hurt(inflictor: IEntity, energy: number, name: string) {
    //dont care foo, i'm a wall!
  }

  constructor(public readonly owner:IGame, radius:number, segments:number, displaySegments:number){
    //world body ...any fixtures thaty apply worldwide...
    this.body = owner.world.world.createBody({type: planck.Body.STATIC, position: new planck.Vec2(0,0), })
    //create walls around world
    this.body.setUserData(this);
    var fix = this.body.createFixture({shape: new planck.Chain(this.BuildBoundary(radius, segments), true), restitution: 1})
    fix.setFilterData({ 
      groupIndex: 0, 
      categoryBits:CollisionGroup.boundary,
      maskBits: CollisionGroup.all 
    })

    var path = [ new BABYLON.Vector3(0,0,-100), new BABYLON.Vector3(0,0,100)]

    //visual representation of walls
    this.mesh = MeshBuilder.CreateTube("worldwall", {
      path:path,
      radius:radius, 
      tessellation:displaySegments, 
      sideOrientation: BABYLON.Mesh.DOUBLESIDE
    }, owner.scene)

    owner.scene.addMesh(this.mesh)
    this.mesh.setAbsolutePosition(new BABYLON.Vector3(0,0,0))


    //const mat = new BABYLON.PBRSpecularGlossinessMaterial("boundary_mat", owner.scene)
    

    const mat = new BABYLON.StandardMaterial("boundarymat", owner.scene)
    mat.alpha= 0.2
    mat.diffuseColor = new BABYLON.Color3(1, 0, 1);
    mat.specularColor = new BABYLON.Color3(0.5, 0.6, 0.87);
    mat.emissiveColor = new BABYLON.Color3(0.4, 0.6, 1);
    mat.ambientColor = new BABYLON.Color3(0.23, 0.98, 0.53);

    this.mesh.material = mat

  }

  PrePhysics: () => boolean;  PreDraw: () => void
  meshY: () => 0
  meshX: () => 0
  mass: () => 0
  pos(){ return CircleBoundary.Zero}
  vel(){ return CircleBoundary.Zero}
  ang: () => 0
  
  CaclulateGravity(target: IEntity): void {
    throw new Error("Method not implemented.")
  }
  AddForce(force: IV2): void {
    throw new Error("Method not implemented.")
  }

  Collision(other: IEntity): void {
    //console.log(other.name + " hit the wall") 
  }

  readonly name: string ="boundary"

  protected BuildBoundary(radius:number, segments:number):Array<V2>{
    var step = 2 * Math.PI / segments;
    let res = new Array<V2>(32);
    for(let i = 0; i < segments; i++){
      res[i] = new V2(0,0).setAngle(step * i, radius);
    }
    return res;
  }
}