import { Ship } from "./ship";
import { Command, ControlMan } from "../controlman";
import { IGame, IEntity } from "../interfaces";
import { V2 } from "../helpers/v2";
import { Utils } from "../helpers/utils";


namespace PlayerControls{
  export const PlayerLeft =  new Command("P1_TURN_LEFT","LEFT",37)
  export const PlayerRight = new Command("P1_TURN_RIGHT","RIGHT",39)
  export const PlayerThrust = new Command("P1_THRUST","THRUST", 38) 
  export const PlayerShoot = new Command("P1_SHOOT","SHOOT",32)
  export const PlayerCycle = new Command("P1_CYCLE","CYCLE",40)
  ControlMan.registerCommands([ PlayerLeft, PlayerRight, PlayerThrust, PlayerShoot, PlayerCycle]);
}

export class LocalPlayer extends Ship implements IEntity{

  public constructor(name:string, owner:IGame, pos:V2, vel:V2){
    super(name, owner, pos,vel)
    ///control bindings
    PlayerControls.PlayerThrust.Subscribe((active, name) =>{
      this.thrust = active ? 1 : 0 
    })
    PlayerControls.PlayerLeft.Subscribe((active, name) =>{ 
      this.turn += active ? -1 : 1
      this.turn = Utils.unitize(this.turn)
    })
    PlayerControls.PlayerRight.Subscribe((active, name) =>{ 
      this.turn += active ? 1 : -1 
      this.turn = Utils.unitize(this.turn)
    })
    PlayerControls.PlayerShoot.Subscribe((active)=> {
        this.shoot = active
    })
    PlayerControls.PlayerCycle.Subscribe((active, name) =>{ 
      this.thrust = active ? -1 : 0 
    })
  }



}