import { RotatingEntity } from "./rotatingentity";
import { ILoader, IGame, IV2, IEntity } from "../interfaces";
import { V2 } from "../helpers/v2";
import { Entity } from "./entity";
import { PBRBaseMaterial, PBRMaterial, IncrementValueAction } from "babylonjs";
import { Pool, IPooledItem, IPool, PooledItem } from "../helpers/pool";
import { applyMixins } from "../helpers/mixins";
import { Constants, ParticleType } from "../constants";
import { Killable } from "./killable";
import { Splosion } from "../effects/splosion";
import { constants } from "os";
import { CollisionGroup } from "../constants";



export namespace AsteroidConstants{
  export const Density = 1
  export const Restitution = 0.1
  export const Health = 5000
}


export type AsteroidCallback = (roid:IEntity)=>void

export interface AsteroidOptions{
  scale:number,
  onDeath?:AsteroidCallback,
  spawnInvulnerabilityTime?:number
}

export class AsteroidPool extends Pool<Asteroid>{

  private static readonly defaultOptions:AsteroidOptions = {
    scale:1,
    onDeath:null,
    spawnInvulnerabilityTime:0
  }

  constructor(owner:IGame, protected namePrefix:string = "asteroid_", customOptions:AsteroidOptions = null){
    super(owner, namePrefix, Asteroid.GetNew, customOptions, AsteroidPool.defaultOptions);
  }
}

export class Asteroid extends Killable implements PooledItem,IPooledItem<Asteroid>{

  liveTime:number 
  invulnerable:boolean
  pool:IPool = null
  Free: ()=>void
  protected readonly fixture:planck.Fixture

  protected constructor(name:string, owner:IGame, pool:IPool, protected readonly options:AsteroidOptions){
    super(name, owner, false)
    this.pool = pool
    this.mesh = Asteroid.asteroidMesh.clone(name+"mesh", owner.scene.rootNodes[0])
    this.mesh.scaling.set(options.scale, options.scale, options.scale)
    this.mesh.setEnabled(false)
    this.body = owner.world.world.createBody({  type: planck.Body.DYNAMIC })
    this.fixture = this.body.createFixture({
      shape: new planck.Circle(new planck.Vec2(0,0),4 * options.scale),
      density:AsteroidConstants.Density,
      restitution:AsteroidConstants.Restitution
    })
    this.body.setUserData(this)
    this.body.setActive(false)
  }

  public static GetNew(name:string, owner:IGame, pool:IPool, options:AsteroidOptions = null):Asteroid{
    return new Asteroid(name, owner, pool, options);
  }


  protected Killed(killer:IEntity, name:String){
    super.Killed(killer, name)
    this.owner.splosionPool.GetNew().Init(this, 100, ParticleType.Rubble)
  }


  CleanUp(){

    this.mesh.setEnabled(false)
    this.body.setActive(false)
    if (this.options.onDeath){
      this.options.onDeath(this)
    }
  }


  protected static asteroidMesh:BABYLON.AbstractMesh
 
  public static loader():ILoader {
    return{
      preload: (assMan:BABYLON.AssetsManager)=>{
        assMan.addMeshTask("asteroid","","/assets/", "asteroid.gltf").onSuccess = (task)=>{
          this.asteroidMesh = task.loadedMeshes.filter(v=>v.id ==="asteroid1")[0]; 
          //(this.asteroidMesh.material as PBRMaterial).usePhysicalLightFalloff = false;
        }
      } 

    }
  }


  public Init(pos:V2, vel:V2, angvel:number){
    this.body.setPosition(pos)
    this.body.setAngularVelocity(angvel)
    this.body.setLinearVelocity(vel)
    this.body.setAngle(Math.random() * 2 * Math.PI)
    this.owner.scene.addMesh(this.mesh)
    this.body.setActive(true)
    this.owner.entman.RegisterEntity(this, false)
    this.health = AsteroidConstants.Health * this.options.scale

    this.liveTime = 0
    this.invulnerable = this.options.spawnInvulnerabilityTime > 0
    this.fixture.setFilterData({ 
      groupIndex: 0, 
      categoryBits:CollisionGroup.asteroid,
      maskBits: this.invulnerable ?  (CollisionGroup.boundary | CollisionGroup.star) :  CollisionGroup.all 
    })
    this.alive = true
    this.mesh.setEnabled(true)
    //console.log(`${this.name} initiated`)

  }


  public PrePhysics():boolean{
    this.liveTime += Constants.TickTime
    if (this.liveTime > this.options.spawnInvulnerabilityTime && this.invulnerable){
      this.fixture.setFilterData({ groupIndex: 0, categoryBits:CollisionGroup.asteroid, maskBits:CollisionGroup.all})

    }
    if (!this.alive){
      this.Free()
      return false
    }
    return super.PrePhysics()

  }

  private static relativeVel:V2 = new V2()

  public Collision(other: IEntity): void{
    //this.alive = false
    //calc KE
    Asteroid.relativeVel.setV2(this.vel()).sub(other.vel())
    var ke= 0.5 * this.mass() * Asteroid.relativeVel.len2()
    other.Hurt(this, ke, "collision")
  }

}




applyMixins(Asteroid,[PooledItem])