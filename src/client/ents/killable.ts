import { Entity } from "./entity";
import { IEntity } from "../interfaces";

export abstract class Killable extends Entity{

  protected health:number = 100

  public Hurt(inflicter:IEntity, energy:number, name:string ):void{
    
    console.log(`${this.name} took ${energy} pain from ${inflicter.name}`)
    this.health -= energy
    if (this.health < 0){
      this.Killed(inflicter, name)
    }
  }

  protected Killed(killer:IEntity, name:String){
    console.log(`${this.name} ${name} by ${killer.name}`)
    this.alive = false
  }
}