import { Entity } from "./entity";
import { IGame, IEntity } from "../interfaces";
import { V2 } from "../helpers/v2";
import { Constants } from "../constants";
import { OculusTouchController } from "babylonjs";
import { CollisionGroup } from "../constants";



export namespace StarConsts{
  export const firecols = [new BABYLON.Color3(1,1,0.5),new BABYLON.Color3(0.9,0.5,0),new BABYLON.Color3(0.5,0.5,0),new BABYLON.Color3(1,0.9,0), new BABYLON.Color3(0.5,0.5,0.1), new BABYLON.Color3(0.9,0.9,0.0)]
  

}

export class Star extends Entity{

  light:BABYLON.PointLight
  fixture:planck.Fixture
  public constructor(name:string, owner:IGame, pos:V2, protected radius:number, density:number, protected intensity:number, colours:BABYLON.Color3[] = StarConsts.firecols, vel:V2 = null, angvel:number = 1.5){
    super(name, owner)
    this.owner.entman.RegisterEntity(this, true)
    //var firemat = new BABYLON.FireMaterial("fire1", owner.scene)

    this.mesh = BABYLON.MeshBuilder.CreateSphere("star1", { segments:12, diameter:(radius * 2), updatable:false}, owner.scene)
    
    var fireMaterial = new BABYLON.StandardMaterial(name + "_texture", owner.scene)
    var fireTexture = new BABYLON.FireProceduralTexture(name +"_fire", 256, owner.scene, null,false)
    //fireTexture.fireColors = [new BABYLON.Color3(1,1,0),new BABYLON.Color3(1,1,0),new BABYLON.Color3(1,1,0), new BABYLON.Color3(1,0.5,0.25), new BABYLON.Color3(1,0,0), new BABYLON.Color3(0,0,0)]
    //fireTexture.speed = new BABYLON.Vector2(3,0)
    fireTexture.fireColors = colours
    fireTexture.uScale = fireTexture.vScale = 1

//    fireMaterial.specularPower = 0
    fireMaterial.emissiveTexture = fireTexture
    fireMaterial.disableLighting=true
    this.mesh.material = fireMaterial


    this.light = new BABYLON.PointLight(name+"_light", pos.toVect3(), this.owner.scene)
    this.light.intensity = intensity
    this.light.diffuse = colours[0]
    this.light.radius =this.radius
    owner.scene.addMesh(this.mesh)
    this.body = owner.world.world.createBody({ 
      type: planck.Body.DYNAMIC,
      position: pos, 
      angle:0  })
    this.body.setUserData(this)

    this.fixture = this.body.createFixture({shape: new planck.Circle(new planck.Vec2(0,0),  this.radius), density:density, restitution:0.1})
    this.fixture.setFilterData( { groupIndex: 0, categoryBits:CollisionGroup.star, maskBits: CollisionGroup.all} )

    if (vel !== null){
      this.body.setLinearVelocity(vel)
    }

    this.body.setAngularVelocity(angvel)
  }

  public PreDraw(): boolean { 
    super.PreDraw()
    this.light.position.copyFrom(this.mesh.position)
    return true
  }

  public Collision(other:IEntity){
    other.Hurt(this, 20000, "Sunburnt")
  }
    /*
    //do nothing if we're already dead
    if (!this.alive) return
    switch (other.EntType()){
      case Constants.EntityType.Star:
        this.ResolvsStarCollision(other as Star)
        break;

      default:
      //do nothiong for now
        break;
    }
    */

  protected ResolvsStarCollision(other:Star):void{
    other.alive = false
    //average densities
    var density = (this.fixture.getDensity() + other.fixture.getDensity()) / 2
    var mass = this.mass() + other.mass()
    //calculate radius at our average density
    let newRadius = Math.sqrt(mass / density) / Math.PI
    this.fixture.getShape().m_radius = newRadius
    this.fixture.setDensity(density)
    this.mesh.scaling.scale( newRadius / this.radius)
  }


}