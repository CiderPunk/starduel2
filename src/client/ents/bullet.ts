import {  IGame, IV2, IEntity, IBulletPool, IBullet, IShooter } from "../interfaces";
import { V2 } from "../helpers/v2";
import { Entity } from "./entity";
import { Pool, IPooledItem, IPool, PooledItem } from "../helpers/pool";
import { Constants } from "../constants";
import { applyMixins } from "../helpers/mixins";
import { CollisionGroup } from "../constants";



export class BulletPool extends Pool<Bullet> implements IBulletPool{
  public readonly bulletMesh:BABYLON.Mesh

  constructor(owner:IGame){
    super(owner, "bullet_", Bullet.GetNew)
    //Bullet.LoadAssets(owner)
    let mesh = BABYLON.MeshBuilder.CreateSphere("bullettemplate", {segments:5, slice:1, diameter:1.2}, owner.scene)
    mesh.setEnabled(false)
    this.bulletMesh = mesh
  }
}

export class Bullet extends Entity implements PooledItem, IPooledItem<Bullet>, IBullet {
  
  pool:IPool = null
  Free:()=>void
  protected lifeTime:number
  alive:boolean
  protected fixture:planck.Fixture
  //protected static bulletMesh:BABYLON.Mesh;
 
  /*
  public static LoadAssets(game:IGame):void{
    Bullet.bulletMesh = BABYLON.MeshBuilder.CreateSphere("bullettemplate", {segments:5, slice:1, diameter:1.2}, game.scene)
    Bullet.bulletMesh.setEnabled(false)
  }
*/
  public static GetNew(name:string, owner:IGame, pool:IPool ):Bullet{
    return new Bullet(name, owner, pool)
  }

  CleanUp(){
    this.owner.splosionPool.GetNew().Init(this,20)
    this.mesh.setEnabled(false)
    this.body.setActive(false)
  }

  protected shooter:IShooter

  public PrePhysics():boolean{
    if (!this.alive){
      this.Free()
      return false
    }

    this.lifeTime+=Constants.TickTime
    if (this.lifeTime > 0.2){
      this.fixture.setFilterData({ groupIndex: 0, categoryBits:0x0001, maskBits:0xffff})
    }
    return super.PrePhysics();
  }

  protected constructor(name:string, owner:IGame, pool:IPool){
    super(name, owner, false)
    this.pool = pool
    this.mesh = (pool as BulletPool).bulletMesh.createInstance(name+"_mesh")
    this.owner.scene.addMesh(this.mesh)
    
    this.body = owner.world.world.createBody({  type: planck.Body.DYNAMIC })
    this.body.setBullet(true)
    this.fixture = this.body.createFixture({shape: new planck.Circle(new planck.Vec2(0,0),0.6), density:1, restitution:0.1})
    this.body.setUserData(this)
    //pooled entities gotta start innactive
    this.body.setActive(false)
    this.mesh.setEnabled(false)
  }

  public Init(pos:V2, vel:V2, density:number,  shooter:IShooter){
    this.shooter = shooter
    this.body.setPosition(pos)
    this.body.setLinearVelocity(vel)
    this.body.setActive(true)
    //adjust mass for extra bangs
    this.fixture.setDensity(density)
    this.fixture.setFilterData({ groupIndex: shooter.groupId, categoryBits:CollisionGroup.projectile, maskBits:CollisionGroup.all})
    //this.owner.scene.addMesh(this.mesh)
    this.owner.entman.RegisterEntity(this, false)
    this.lifeTime = 0
    this.alive = true
    this.mesh.setEnabled(true)
    //this.mesh.visibility = 1
  }

  private static relativeVel:V2 = new V2()

  public Collision(other: IEntity): void{
    this.alive = false
    //calc KE
    Bullet.relativeVel.setV2(this.vel()).sub(other.vel())
    var ke= 0.5 * this.mass() * Bullet.relativeVel.len2()
    other.Hurt(this.shooter,  ke, "shot")
    console.log(`${this.name} hit ${other.name}`) 
  }
}

applyMixins(Bullet,[PooledItem])