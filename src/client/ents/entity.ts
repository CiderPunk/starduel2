import { IEntity, IGame, IV2 } from "../interfaces";
import { V2 } from "../helpers/v2";
import { Constants } from "../constants";

export abstract class Entity implements IEntity{


  public Hurt(inflictor: IEntity, energy: number,name:string):void{}

  public Collision(other: IEntity): void{
    //splode or something
    //console.log(this.name + " hit " + other.name)
  }

  private static readonly scratch:V2 = new V2(0,0)

  public CaclulateGravity(target: IEntity): void {
    //get distance
    Entity.scratch.set(target.body.getPosition()).sub(this.body.getPosition())
    var force = Constants.GravityConstant * ((target.body.getMass() * this.body.getMass()) / Entity.scratch.len2())
    Entity.scratch.norm().mul(force)
    this.AddForce(Entity.scratch)
    target.AddForce(Entity.scratch.invert())
  }


  AddForce(force:IV2): void {
    this.cumulativeforce.add(force)
  }
  
  public PrePhysics():boolean{

    if (this.cumulativeforce.hasValue){
      this.body.applyForceToCenter(this.cumulativeforce)
      this.cumulativeforce.reset()
    }
    return this.alive
  }

  //protected owner:IGame
  protected mesh:BABYLON.AbstractMesh
  public body:planck.Body

  public mass = ():number => this.body.getMass()
  public pos = ():IV2 => this.body.getPosition()
  public vel = ():IV2 => this.body.getLinearVelocity()
  public ang = ():number => this.body.getAngle()

  public meshY = ():number => this.mesh.absolutePosition.y
  public meshX = ():number => this.mesh.absolutePosition.x

  protected cumulativeforce:V2 =new V2(0,0)
  protected static tempv3 = new BABYLON.Vector3()

  protected alive:boolean = true

  public PreDraw():void { 
    var pos = this.body.getPosition()
    this.mesh.setAbsolutePosition(Entity.tempv3.set(pos.x, pos.y, 0));
    var ang = this.body.getAngle();

    if (this.mesh.rotationQuaternion!= null){
      this.mesh.rotationQuaternion.set(0,0,Math.cos(ang / 2), Math.sin(ang/2)) 
    }
    else{
      this.mesh.rotate(BABYLON.Axis.Z, ang, BABYLON.Space.LOCAL);
    }
  }

  public constructor(public readonly name:string, public readonly owner:IGame, heavy:boolean = false ){
    //this.owner.entman.RegisterEntity(this, heavy)
  }
  
  public Name = ():string =>this.name;

}