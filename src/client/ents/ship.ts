
import { ILoader, IGame, IEntity, IV2, IWeapon, IShooter } from '../interfaces';
import { Command, ControlMan } from '../controlman';
import { Utils } from '../helpers/utils';
import { V2 } from '../helpers/v2';
import { Entity } from './entity';
import { Cannon } from '../weapons/cannon';
import { BurstShot } from '../weapons/burstshort';
import { Constants } from '../constants';
import { Killable } from './killable';
import { CollisionGroup } from "../constants";


namespace ShipConst{
  export const THRUST_MULITPLIER = 80
  export const TORQUE_MULITPLIER = 700
  export const ANGULAR_DAMPING = 3
  export const SHIP_DENSITY = 0.1
  export const SHIP_RESTITUTION = 0.5


  export const SHIP_HEALTH = 100000
}



export class Ship extends Killable implements IShooter{

  protected static groupCounter:number = -1

  protected static readonly dir = new V2();

  public PrePhysics():boolean {
    this.body.applyTorque(ShipConst.TORQUE_MULITPLIER * this.turn);
    Ship.dir.setAngle(this.body.getAngle(), ShipConst.THRUST_MULITPLIER * this.thrust);
    this.body.applyForceToCenter(Ship.dir)

    if (this.weapon){
      this.weapon.Update(this.shoot)
    }

    return super.PrePhysics()
  }

  
  protected static shipMesh:BABYLON.AbstractMesh;


  protected arsenal:IWeapon[]


  public static loader():ILoader {
    return{
      preload: (assMan:BABYLON.AssetsManager)=>{
        assMan.addMeshTask("ship","","/assets/", "ship.gltf").onSuccess = (task)=>{
          this.shipMesh = task.loadedMeshes.filter(v=>v.id ==="ship1")[0]
          //(this.shipMesh.material as PBRMaterial).usePhysicalLightFalloff = false;
        }
      } 
    }
  }

  protected turn:number = 0
  protected thrust:number = 0
  protected shoot:boolean = false
  protected weapon:IWeapon
  public readonly groupId:number

  public setWeapon(weapon:IWeapon){
    this.weapon = weapon
  }

  public constructor(name:string, owner:IGame, pos:V2, vel:V2){
    super(name, owner)
    this.health = ShipConst.SHIP_HEALTH
    this.groupId = Ship.groupCounter--
    this.owner.entman.RegisterEntity(this, false)
    this.mesh = Ship.shipMesh.clone("ship1", owner.scene.rootNodes[0])
    owner.scene.addMesh(this.mesh)
    this.mesh.position.set(pos.x,pos.y,0)
    //this.mesh.scaling = new Vector3(100 ,100,100);
    this.body = owner.world.world.createBody({  type: planck.Body.DYNAMIC,
       position: new planck.Vec2(pos.x,pos.y), 
       angle:0  })
    this.body.setUserData(this);
    this.body.setLinearVelocity(vel)
    //var verts = [new planck.Vec2(0,-13.5), new planck.Vec2(7,-2), new planck.Vec2(10,6), new planck.Vec2(-10,6),new planck.Vec2(-7,-2),];
    var verts = [new planck.Vec2(0,-5.2), new planck.Vec2(3,-1.3), new planck.Vec2(5,4.2), new planck.Vec2(-5,4.2),new planck.Vec2(-3,-1.3),];
    
    var fixture = this.body.createFixture({shape: new planck.Polygon(verts), density:ShipConst.SHIP_DENSITY, restitution:ShipConst.SHIP_RESTITUTION})

    fixture.setFilterData({groupIndex: this.groupId, categoryBits:CollisionGroup.ship, maskBits:CollisionGroup.all })
     
    //this.body.createFixture({shape: new planck.Circle(new planck.Vec2(0,0),12), density:0.01, restitution:0.5})
    //this.body.setActive(true)
    this.body.setSleepingAllowed(false)
    this.body.setAngularDamping(ShipConst.ANGULAR_DAMPING)




    //this.setWeapon(new BurstShot(this,{}))
    this.setWeapon(new Cannon(this))
  }
}