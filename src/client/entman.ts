import { IEntityMan, IEntity } from "./interfaces";
import { FastArray } from "./helpers/fastarray";

export class EntityMan implements IEntityMan{

  
  protected heavies = new FastArray<IEntity>()
  protected entities = new FastArray<IEntity>()
  protected scratch = new FastArray<IEntity>()

  public RegisterEntity(target:IEntity, heavy:boolean = false):void{
    if (heavy){
      this.heavies.push(target)
    }
    else{
      this.entities.push(target)
    }
  }

  public PreDraw(): void {
    this.preDrawList(this.entities)
    this.preDrawList(this.heavies)
  }

  public PrePhysics():void{

    //gravity!
    let ent:IEntity;
    while(ent = this.heavies.pop()){
      this.scratch.push(ent)
      this.heavies.forEach((e)=>{ ent.CaclulateGravity(e) })
      this.entities.forEach((e)=>{ ent.CaclulateGravity(e) })
    }
    this.heavies.swap(this.scratch);

    this.prePhysicList(this.entities)
    this.prePhysicList(this.heavies)
  }


  private preDrawList(set:FastArray<IEntity>):void{
    let ent:IEntity;
    set.forEach((ent)=>{ent.PreDraw()})
  }
  
  private prePhysicList(set:FastArray<IEntity>):void{
    let ent:IEntity;
    while(ent = set.pop()){
      if (ent.PrePhysics()){
        this.scratch.push(ent);
      }
    }
    set.swap(this.scratch);
  }
 

}