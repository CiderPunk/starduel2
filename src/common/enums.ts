export enum GameState {
  Register,
  Game,
  Practice,
  Wait,
  Lobby,
}

export enum JoinResult{
  Success,
  NoAccount,
  AuthError,
  AlreadyConnected,
}

export enum ChallengeStatus {
  UserNotFound,
  AlreadyChallenged,
  Pending,
  Accepted,
  Blocked,
  TimeOut,
  Disconnected,
  Declined
}