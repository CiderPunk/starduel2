export namespace Spec{

  export interface GameSpec{
    radius:number,
    ents:Array<EntSpec>
  }

  export interface EntSpec{
    name:string,
    entType:string,
    entSubType:string,
    pos:IV2,
    vel:IV2,
    av:number,
    data?:object,
  }

  export interface StarSpec{
    density:number,
    intensity:number,
    radius:number,
    colour:string,
  }

  export interface IV2{
    x:number,
    y:number,
  }

  export namespace Size{
    export const Large = "large"
    export const Medium = "medium"
    export const Small = "small"
  }

  export namespace StarColours{
    export const Fire = "fire"
    export const Purple = "purple"
    export const Acid = "acid"
    export const Blue = "blue"
  }

  export namespace EntTypeNames{
    export const Asteroid = "asteroid"
    export const Star = "star"
    export const Player = "player"
  }
}