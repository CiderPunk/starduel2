import { JoinResult, GameState, ChallengeStatus } from "./enums";

export namespace Comms{
  


  export interface ChangeStatus{
    state:GameState
  }

  export interface JoinRequest{
    name:string
    uuid:string
    hash:string
    lobbyId?:string
  }

  export interface JoinResponse{
    result:JoinResult
    message?:string
  }
  

  export interface JoinLobbyRequest{
    name?:string 
  }


  export interface LobbyDetails{
    name:string
    uuid:string
    users:Array<UserDetails>
  }

  
  export interface RegisterRequest{
    name:string
  }

  export interface RegisterResponse{
    name:string
    uuid:string
    hash:string
  }

  export interface UserDetails{
    name:string 
    uuid:string
    state:GameState
  }

  export interface Challenge{
    uuid:string
    status:ChallengeStatus
  }

  export interface InitGameComm{
    initiator:boolean
  }

  export interface SignalGameComm{
    data:any
  }


  export interface Ping{
    requestTime:number
    responseTime:number
  }
}


export namespace EventNames{
  export const StatusUpdate = "stup"
  export const JoinRequest = "jrq"
  export const JoinResponse = "jrp"
  export const RegisterRequest = "rgrq"
  export const RegisterResponse = "rgrp"
  export const JoinLobbyRequest = "lbrq"
  export const PushedLobby = "lpsh"
  export const LobbyUpdate = "lupd"
  export const ChallengeRequest = "chrq"
  export const ChallengeResponse = "chrp"
  export const Challenged = "chd"
  export const InitGC = "igc"
  export const SignalGC = "sgc"
  export const Ping = "ping"
  export const Pong = "pong"

}



